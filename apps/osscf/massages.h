#ifndef MASSAGE_H
#define MASSAGE_H

enum {
	AM_CONFIGURATIONPACKET = 1,
	NIL = 0,
	PACKAGE_REPEATING = 2,
	MAX_INT = 3000,
	SENSORMSG = 99,
	BEACON_PACKET = 10000,
	PARENT_STATUS_QUERY = 10001,
	PARENT_DISCOVERY = 10002,
	ED_SENSOR_REPORT = 10003,
	ACCESS_POINT_SENSOR_REPORT = 10004,
	REQUEST_VAL = 10005,
 	CONFIRM_VAL = 10006,
	RESPONS_VAL = 10007,
	PSQ_REQUEST = 10008,
	PSQ_RESPONS = 10009,
	ED_JOIN_REQUEST_RESPONSE = 10010,
	CONFIGURATION_PACKET = 10011
};

typedef nx_struct BeaconPacket {
	nx_uint16_t pkt_type;
	nx_uint8_t gradientVal;
	nx_uint8_t senderID;
} BeaconPacket;

typedef nx_struct ParentStatusQuery {
	nx_uint16_t pkt_type;
	nx_uint8_t parentID;
} ParentStatusQuery;

typedef nx_struct ParentDiscovery {
	nx_uint16_t pkt_type;
	nx_uint8_t senderID;
} ParentDiscovery;

typedef nx_struct EDJoinRequestResponse {
	nx_uint16_t pkt_type;
	nx_uint16_t f_n_ID;
	nx_uint8_t ED_nodeID;
} EDJoinRequestResponse;

typedef nx_struct EDSensorReport {
	nx_uint16_t pkt_type;
	nx_uint8_t ED_nodeID;
	nx_uint8_t roomID;
	nx_uint16_t sensorValue[4];
} EDSensorReport;

typedef nx_struct AccessPointSensorReport {
	nx_uint16_t pkt_type;
	nx_uint8_t nextForwardingNode;
	nx_uint8_t currForwardingNode;
	nx_uint8_t senderID;
	nx_uint8_t floorID;
	nx_uint8_t roomID;
	nx_uint8_t ED_nodeID;
	nx_uint16_t sensorValue[3];
} AccessPointSensorReport;

typedef nx_struct ConfigurationPacket {
	nx_uint16_t pkt_type;
	nx_uint8_t nodeID;
	nx_uint16_t sensorInterval;
	nx_bool light;
	nx_bool temperature;
	nx_bool humidity;
} ConfigurationPacket;

typedef nx_struct ConfigurationPacketRespons {
	nx_uint16_t pkt_type;
	nx_uint8_t senderID;
	nx_uint8_t parentID;
} ConfigurationPacketRespons;

typedef nx_struct BaseStationReport {
	nx_uint8_t nodeData[2];
	nx_uint16_t sensorValue[2][3];	
} BaseStationReport;

#endif /* MASSAGE_H */
