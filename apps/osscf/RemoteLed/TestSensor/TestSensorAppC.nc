configuration TestSensorAppC {

} implementation {
	components TestSensorP;
	components new TimerMilliC() as TimerSensor;
	components MainC;
	TestSensorP.Boot -> MainC;

	components SerialPrintfC;

	components new SensorHumidityC() as SensorHumidity;
	components new SensorLightC() as SensorLight;
	components new SensorTemperatureC() as SensorTemperature;

	TestSensorP.Temperature -> SensorTemperature;
	TestSensorP.Light -> SensorLight;
	TestSensorP.Humidity -> SensorHumidity;
	TestSensorP.TimerSensor -> TimerSensor;
}