#include <stdio.h>
#include <stdint.h>

module TestSensorP @safe() {
	uses interface Boot;
	uses interface Read<uint16_t> as Light;
	uses interface Read<uint16_t> as Humidity;
	uses interface Read<uint16_t> as Temperature;
	uses interface Timer<TMilli> as TimerSensor;
  
} implementation {
	event void Boot.booted()  {
		//call TimerSensor.startPeriodic(5000);
		call Light.read() ;
	}
	
	event void TimerSensor.fired() {
	
		
	}
  
	void sleep1milisec(){
		int i = 1000000;
		while (i-- > 0);
	}
	
	void sleep(int milisecs){
		int i;
		for (i = 0; i < milisecs; i++)
			sleep1milisec();
	}
  
	event void Light.readDone(error_t result, uint16_t data) {
		if (result != SUCCESS) {			
			printf("Problem in get light\n");
		}else {

			printf("Got light %d\n", data);
		}
		sleep(2000);
		call Temperature.read() ;
		
  	}
  	
	event void Temperature.readDone(error_t result, uint16_t data) {
		if (result != SUCCESS) {			
			printf("Problem in get Temperature\n");
		}else {

			printf("Got Temperature %d\n", data);
		}
		sleep(2000);
		call Humidity.read() ;
  	}
  	
  	event void Humidity.readDone(error_t result, uint16_t data) {
		if (result != SUCCESS) {			
			printf("Problem in get Humidity\n");
		}else {
			printf("Got Humidity %d\n", data);
		}
		sleep(2000);
		call Light.read() ;
  	}
}
