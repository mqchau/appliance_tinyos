import SocketServer
import ReadSensor
import FindIP
import re

ReadSensorObj = None

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
		global ReadSensorObj
        # self.request is the TCP socket connected to the client
		self.msg = self.request.recv(1024).strip()
		if re.search(r'get_data', self.msg):      
			self.request.sendall(ReadSensorObj.getData())
		elif re.search(r'set_gio', self.msg):
			#format of this command: set_gio?id=3&pin=0&val=1
			#find id
			m = re.search("id=(\d+)", string)
			node_id = int(m.group(1))
			#find pin
			m = re.search("pin=(\d+)", string)
			pin_id = int(m.group(1))
			#find val
			m = re.search("val=(\d+)", string)
			val = int(m.group(1))
			if pin_id < 0 or pin_id > 3 or val < 0 or val > 1:
				self.request.sendall("Invalid command")
			else:
				gio_status_struct = {pin_id : val}
				ReadSensorObj.setGio(node_id, gio_status_struct);
		else:
			self.request.sendall("Invalid command")
			
			
if __name__ == "__main__":
	try:	

		global ReadSensorObj
				
		HOST, PORT = FindIP.find_ip(), 2324	

		# Create the server, binding to localhost on port 9999
		server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
		
		ReadSensorObj = ReadSensor.ReadSensor()
		ReadSensorObj.start()
		 
		
		# Activate the server; this will keep running until you
		# interrupt the program with Ctrl-C
		server.serve_forever()

	except KeyboardInterrupt:
		ReadSensorObj.stop()
		server.shutdown()
	