import serial,threading, re, json, time
from collections import deque
 
DEBUG = True
	  
class ReadSensor(threading.Thread):
	DEVICE = '/dev/ttyUSB0'
	BAUD_RATE = 57600
	
	max_return_array_length = 5
	
	StopFlagLock = threading.RLock()
	StopFlag = False
	
	DataLock = threading.RLock()
	DataFile = dict()
	
	SerialBusyLock = threading.RLock()
	
			
	def __init__(self):
		threading.Thread.__init__(self)
		self.serial_port = serial.Serial(self.DEVICE, self.BAUD_RATE, timeout=5)
		self.current_gio_state = dict()			
		self.clearAllData()
		
		
	def __exit__(self):
		threading.Thread.__exit__(self)
		self.serial_port.close()
		self.clearAllData()
	
	def clearAllData(self):
		#clear all data
		for one in self.DataFile.keys():
			self.clearData(one)
			
	def appendData(self, data_struct):
		with self.DataLock:
			if data_struct['sensorid'] not in self.DataFile:
				self.DataFile[data_struct['sensorid']] = {
						'light': 	deque()
					,	'temp': 	deque()
					,	'humid':	deque()
				}
		
			for keyword in self.DataFile[data_struct['sensorid']].keys():
				if len(self.DataFile[data_struct['sensorid']][keyword]) >= self.max_return_array_length:
					self.DataFile[data_struct['sensorid']][keyword].popleft()
				self.DataFile[data_struct['sensorid']][keyword].append(data_struct[keyword])	
			
	def clearData(self, sensorid):
		with self.DataLock:
			for array in self.DataFile[sensorid].values():
				array.clear()
			
	def run(self):
		# if DEBUG:
			# while True:	
				# with self.StopFlagLock:
					# LocalStopFlag = self.StopFlag
				# if LocalStopFlag:
					# break
					
				# #put sample in array
				# import random				
				# self.appendData('temp', random.randint(1, 100))			
				# self.appendData('light', random.randint(101, 200))			
				# self.appendData('humid', random.randint(201, 300))
				# time.sleep(0.5)	
				# print "generate new data"
		# else:
			while True:
				
				if self.checkStopFlag():
					break
					
				#read data
				line = ''
				while len(line) == 0 and not self.checkStopFlag():
					with SerialBusyLock:
						line = self.serial_port.readline().strip()
				
				if DEBUG:
					print line
					
				#parse it
				data_struct = self.parseSensorString(line)
				
				if DEBUG:
					print json.dumps(data_struct)
				
				#put them in array
				self.appendData(data_struct)
				
	
	def checkStopFlag(self):
		with self.StopFlagLock:
			LocalStopFlag = self.StopFlag
		return LocalStopFlag
		
	def stop(self):
		with self.StopFlagLock:
			self.StopFlag = True
			
	def parseSensorString(self, string):
		sensorid_value = int(re.search(r'SensorID=(\d+),', string).group(1))
		light_value = int(re.search(r'Light=(\d+),', string).group(1))
		temp_value = int(re.search(r'Temp=(\d+),', string).group(1))
		humid_value = int(re.search(r'Humid=(\d+),', string).group(1))
		return {
			'sensorid' : sensorid_value,
			'light' : light_value,
			'temp' : temp_value,
			'humid' : humid_value
			}	
	
	def convertData(self):
		return_struct = dict()
		with self.DataLock:
			for one_id in self.DataFile:
				return_struct[one_id] = dict()
				for one_keyword in self.DataFile[one_id]:
					return_struct[one_id][one_keyword] = list(self.DataFile[one_id][one_keyword])
					
		return return_struct			
	
 	def getData(self):		
		return_string = json.dumps(self.convertData())		
		self.clearAllData()
		return return_string

	def setGio(self, node_id, new_state):
		#prepare string to write to serial port
		#format is "GioConfig:NodeID=0003,GIO0=0,GIO1=0,GIO2=1,GIO3=1___________________________________________"
		if not (node_id in self.current_gio_state):		#incase that node isn't connected 
			self.initializeGioState(node_id)
		
		for one_pin in self.current_gio_state[node_id]:
			if not (one_pin in new_state):
				new_state[one_pin] = self.current_gio_state[one_pin]
			else:
				self.current_gio_state[node_id][one_pin] = new_state[one_pin]
		string_to_send = "GioConfig:NodeID=%04d,GIO0=%d,GIO1=%d,GIO2=%d,GIO3=%d___________________________________________" %
			(self.current_gio_state[node_id]['gio0'], self.current_gio_state[node_id]['gio1'], self.current_gio_state[node_id]['gio2'], self.current_gio_state[node_id]['gio3'])
		with SerialBusyLock:	
			self.serial_port.write(string_to_send)
		
	def initializeGioState(self, node_id):
		self.current_gio_state[node_id] = {
			'gio0' : 0,
			'gio1' : 0,
			'gio2' : 0,
			'gio3' : 0
			}

if __name__ == "__main__":
	try:
		sensor = ReadSensor()
		sensor.start()
		while True:
			print sensor.getData()
			time.sleep(10)
	except KeyboardInterrupt:
		sensor.stop()
