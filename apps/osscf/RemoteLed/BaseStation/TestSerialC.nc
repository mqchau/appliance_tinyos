// $Id: TestSerialC.nc,v 1.7 2010/06/29 22:07:25 scipio Exp $

/*									tab:4
 * Copyright (c) 2000-2005 The Regents of the University  of California.  
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of the University of California nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (c) 2002-2003 Intel Corporation
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached INTEL-LICENSE     
 * file. If you do not find these files, copies can be found by writing to
 * Intel Research Berkeley, 2150 Shattuck Avenue, Suite 1300, Berkeley, CA, 
 * 94704.  Attention:  Intel License Inquiry.
 */

/**
 * Application to test that the TinyOS java toolchain can communicate
 * with motes over the serial port. 
 *
 *  @author Gilman Tolle
 *  @author Philip Levis
 *  
 *  @date   Aug 12 2005
 *
 **/

#include <Timer.h>
#include "massages.h"
#include "stdlib.h"
#include "stdio.h"

module TestSerialC {
  provides {
    interface Msp430UartConfigure;
  }
  uses {
//    interface SplitControl as SerialControl;
    interface Leds;
    interface Boot;
//    interface Receive as SerialReceive;
//    interface AMSend as SerialSend;
    interface Timer<TMilli> as MilliTimer;
//    interface Packet as SerialPacket;
    
    interface Packet as RadioPacket;
	interface AMPacket as RadioAMPacket;
	interface AMSend as RadioSend;
	interface SplitControl as RadioAMControl;
	interface Receive as RadioReceive;
	
	interface Resource;
//	interface ResourceRequested;
	interface UartStream;
//	interface UartByte;
  }
}
implementation {
	
/*****************************************************************************************
 * Global Variables
*****************************************************************************************/

  enum {
    SERIAL_QUEUE_LEN = 12,
    RADIO_QUEUE_LEN = 12,
  };

  message_t serialQueueBufs[SERIAL_QUEUE_LEN];
  message_t  * ONE_NOK uartQueue[SERIAL_QUEUE_LEN];
  uint8_t    uartIn, uartOut;
  bool       uartBusy, serialFull;
  
  message_t  radioQueueBufs[RADIO_QUEUE_LEN];
  message_t  * ONE_NOK radioQueue[RADIO_QUEUE_LEN];
  uint8_t    radioIn, radioOut;
  bool       radioBusy, radioFull;
 

  bool busyRadio = FALSE;
  bool busySerial = FALSE;
  uint16_t counter;
  uint16_t msgType;
  message_t pkt;
  char uartString[100];
    
  uint8_t ED_nodeID;
  uint16_t ED_temperature;
  uint16_t ED_light;
  uint16_t ED_humidity;
  uint16_t ED_intVolt;

/*****************************************************************************************
 * Task & function declaration
*****************************************************************************************/
  task void requestUART();
  task void releaseUART();

 /*****************************************************************************************
 * Convert Raw Data to SI Units
 * Temperature: 
 * Humidity:
 * Light: lux
 * Internal Voltage: volts
*****************************************************************************************/
/*	float convertLight(uint16_t num) 
	{
		float val = 0;
		//Light
		if (num != 0)
		{
			val = (num / 4096) * 1.5;
			val = (0.625 * 1000000 * num / 100);
		}
		return val;
	}
	float convertTemperature(uint16_t num)
	{
		float val = 0;		
		//Temperature
		if (num != 0)
		{
			val = -39.6 + 0.01 * num;
		}
		return val;
	}
		//Humidity
	float convertHumidity(uint16_t num)
	{
		float val = 0;
		if (num != 0)
		{
			val = -2.0468 + 0.0367 * num + (-1.5955) * 0.000001 * num * num;
		}
		return val;
	}
	float convertVolt(uint16_t num)
	{
		float val;
		//Internal Voltage
		if (num != 0)
		{
			ED_intVolt = ED_intVolt / 4096 * 1.5;
		} 
		return val;
	}
*/
/*****************************************************************************************
 * Boot
*****************************************************************************************/
  
  event void Boot.booted() {
  	uint8_t i;

    for (i = 0; i < SERIAL_QUEUE_LEN; i++)
      uartQueue[i] = &serialQueueBufs[i];
    uartIn = uartOut = 0;
    uartBusy = FALSE;
    serialFull = TRUE;

    for (i = 0; i < RADIO_QUEUE_LEN; i++)
      radioQueue[i] = &radioQueueBufs[i];
    radioIn = radioOut = 0;
    radioBusy = FALSE;
    radioFull = TRUE;
  	
//    call SerialControl.start();
    call RadioAMControl.start();
    post requestUART();
//    call MilliTimer.startPeriodic(8000);
  }

/*****************************************************************************************
 * Uart Configuration
*****************************************************************************************/
  msp430_uart_union_config_t msp430_uart_4800_config = {
    {
      ubr    : UBR_1MHZ_57600,	// Baud rate (use enum msp430_uart_rate_t in msp430usart.h for predefined rates)
      umctl  : UMCTL_1MHZ_57600,	// Modulation (use enum msp430_uart_rate_t in msp430usart.h for predefined rates)
      ssel   : 0x02,		// Clock source (00=UCLKI; 01=ACLK; 10=SMCLK; 11=SMCLK)
      pena   : 0,		// Parity enable (0=disabled; 1=enabled)
      pev    : 0,		// Parity select (0=odd; 1=even)
      spb    : 1,		// Stop bits (0=one stop bit; 1=two stop bits)
      clen   : 1,	        // Character length (0=7-bit data; 1=8-bit data)
      listen : 0,		// Listen enable (0=disabled; 1=enabled, feed tx back to receiver)
      mm     : 0,		// Multiprocessor mode (0=idle-line protocol; 1=address-bit protocol)
      ckpl   : 0,		// Clock polarity (0=normal; 1=inverted)
      urxse  : 0,		// Receive start-edge detection (0=disabled; 1=enabled)
      urxeie : 1,		// Erroneous-character receive (0=rejected; 1=recieved and URXIFGx set)
      urxwie : 0,		// Wake-up interrupt-enable (0=all characters set URXIFGx; 1=only address sets URXIFGx)
      utxe   : 1,		// 1:enable tx module
      urxe   : 1		// 1:enable rx module
    }
  };
  
  async command msp430_uart_union_config_t* Msp430UartConfigure.getConfig() {
    return &msp430_uart_4800_config;
  }

/*****************************************************************************************
 * Timer
*****************************************************************************************/
  
  event void MilliTimer.fired() {
 /* 	
  		call SerialControl.stop();

	  sprintf(uartString,"\rSensorID=%d,light=%d,Temp=%d,Humid=%d,Xaxis=%d,Yaxis=%d,Zaxis=%d"
		,uart_nodeID,uart_light,uart_temperature,uart_humidity,uart_accelerator[0]
		,uart_accelerator[1],uart_accelerator[2]);
	  call Resource.request();	 
	  call UartStream.send(uartString, 100);
	  call Resource.release();
	  call SerialControl.start();
  //    call RadioAMControl.start();
 */ 
  }

/*****************************************************************************************
 * Radio Usage
*****************************************************************************************/

  event message_t* RadioReceive.receive(message_t* msg,void* payload, uint8_t len) {
    if (len == sizeof(ParentStatusQuery)) {
			ParentStatusQuery* receivedPkt = (ParentStatusQuery*)payload;
			if ((receivedPkt->pkt_type == PSQ_REQUEST) && (receivedPkt->parentID == TOS_NODE_ID)) {
					
			} 	
	}
	
	if (len == sizeof(AccessPointSensorReport)) {
			AccessPointSensorReport* receivedPkt = (AccessPointSensorReport*)payload;
			call Leds.led0Toggle();
			if (receivedPkt->pkt_type == ACCESS_POINT_SENSOR_REPORT) {
				call Leds.led1Toggle();
				if(!busySerial) {
					//BaseStationReport* sendPkt = (BaseStationReport*)(call SerialPacket.getPayload(&pkt, sizeof (BaseStationReport)));
					call Leds.led2Toggle();
					
					ED_nodeID = receivedPkt->ED_nodeID;
					ED_light = receivedPkt->sensorValue[0];
					ED_temperature = receivedPkt->sensorValue[1];
					ED_humidity = receivedPkt->sensorValue[2];
					//ED_intVolt = receivedPkt->sensorValue[3];
					
					//post convertUnits();					
					memset(uartString, 0, 100); //clear the string
					sprintf(uartString,"SensorID=%d,Light=%d,Temp=%d,Humid=%d,FD=%d,Xaxis=%d,Yaxis=%d,Zaxis=%d\r\n"
						,ED_nodeID,ED_light,ED_temperature,ED_humidity,0,0,0,0);
//					if(call SerialSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BaseStationReport)) == SUCCESS)
					if(call UartStream.send(uartString, strlen(uartString)) == SUCCESS)
					{
						busySerial = TRUE;
					 	counter++;
					}
		 		}			
			}
	}
		
	return msg;
  }
	
	/*task void radioPSQ()�{
		msgType = PARENT_STATUS_QUERY;
		if(!busyRadio) {
			ParentStatusQuery* sendPkt = (ParentStatusQuery*)(call RadioPacket.getPayload(&pkt, sizeof (ParentStatusQuery)));
			sendPkt->pkt_type = PSQ_RESPONS;
		 	sendPkt->parentID = TOS_NODE_ID;
			if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentStatusQuery)) == SUCCESS){
		 		busyRadio = TRUE;
		 		counter++;
		 	}
		 }
	}*/
	
/*	
  event void SerialSend.sendDone(message_t* bufPtr, error_t error) {
    if (&pkt == bufPtr) {
      busySerial = FALSE;
      //
      if(uart_nodeID != 0)
      {
      					call SerialControl.stop();
					 sprintf(uartString,"\rSensorID=%d,light=%d,Temp=%d,Humid=%d,Xaxis=%d,Yaxis=%d,Zaxis=%d"
					 	,uart_nodeID,uart_light,uart_temperature,uart_humidity,uart_accelerator[0]
					 	,uart_accelerator[1],uart_accelerator[2]);
					 call Resource.request();	 
					 call UartStream.send(uartString, 100);
					 call Resource.release();
					 call SerialControl.start();
	   }//
    }
  }
*/
/*
  event void SerialControl.startDone(error_t error) {
    if (error == SUCCESS) {
      serialFull = FALSE;
    }
  }
*/
//  event void SerialControl.stopDone(error_t err) {}

  event void RadioAMControl.startDone(error_t error){
  	if (error == SUCCESS) {
      radioFull = FALSE;
    }
		msgType = BEACON_PACKET;
		if(!busyRadio) {
			BeaconPacket* sendPkt = (BeaconPacket*)(call RadioPacket.getPayload(&pkt, sizeof (BeaconPacket)));
			sendPkt->pkt_type = BEACON_PACKET;
			sendPkt->gradientVal = 1;
			sendPkt->senderID = TOS_NODE_ID;
			if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BeaconPacket)) == SUCCESS){
	 			busyRadio = TRUE;
	 		}
	 	}
	}

	event void RadioAMControl.stopDone(error_t error){
		// TODO Auto-generated method stub
	}

	event void RadioSend.sendDone(message_t *msg, error_t error){
		if (&pkt == msg) {
			busyRadio = FALSE;
			if(counter < PACKAGE_REPEATING) {
				switch (msgType) {
					case PARENT_STATUS_QUERY : {
							if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentStatusQuery)) == SUCCESS){
								busyRadio = TRUE;
								counter++;
							}
						break;
						}

					case CONFIGURATION_PACKET : {
							
							if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ConfigurationPacket)) == SUCCESS){
								busyRadio = TRUE;
								counter++;
							}
						break;
					}
				}
			} else {
				counter = 0;
			}
		}	
	}
/*
	event message_t * SerialReceive.receive(message_t *msg, void *payload, uint8_t len){
		if (len == sizeof(ConfigurationPacket)) {
			ConfigurationPacket* receivedPkt = (ConfigurationPacket*)payload;
			if (receivedPkt->pkt_type == CONFIGURATION_PACKET ){
					msgType = CONFIGURATION_PACKET;
					if(!busyRadio) {
						ConfigurationPacket* sendPkt = (ConfigurationPacket*)(call RadioPacket.getPayload(&pkt, sizeof (ConfigurationPacket)));
						sendPkt->pkt_type = receivedPkt->pkt_type;
						sendPkt->nodeID = receivedPkt->nodeID;
						sendPkt->sensorInterval = receivedPkt->sensorInterval;
						sendPkt->light = receivedPkt->light;
						sendPkt->humidity = receivedPkt->humidity;
						sendPkt->temperature = receivedPkt->temperature;
						if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ConfigurationPacket)) == SUCCESS){
						 	busyRadio = TRUE;
						 	counter++;
						}
					}
			}
		}
		return msg;
	}
*/
/*****************************************************************************************
 * Uart Usage
*****************************************************************************************/

  task void requestUART() {
    call Resource.request();	// Request UART Resource
  }
	
  task void releaseUART() {
    call Resource.release();	// Never used in this example
  }
  
	event void Resource.granted(){
		// TODO Auto-generated method stub
		serialFull = FALSE;
		call UartStream.receive(uartString, 92);
			
	}

	async event void UartStream.receiveDone(uint8_t *buf, uint16_t len, error_t error){
		// TODO Auto-generated method stub
		busySerial = FALSE;
		if (error == SUCCESS && len == 92 && strstr(buf, "UartConfig") != NULL)
  		{
	  		// local variables
	  		char receivedData[10];
	  		uint16_t uart_nodeID;
	  		uint16_t uart_sensorInterval;
	  		uint8_t uart_light;
	  		uint8_t uart_temperature;
	  		uint8_t uart_humidity;
	  		uint8_t i = 0;  
	  		//copy NodeID		
	  		char* temp = strstr(buf, "NodeID=");
	  		temp = temp + sizeof("NodeID=") - 1;
	  		while (*temp != ',')
	  		{
	  			receivedData[i] = *temp;
	  			temp++;
	  			i++;
	  		}
	  		uart_nodeID = atoi(&receivedData);
	  		
	  		// copy SensorInterval
	  		i = 0;
	  		temp = strstr(buf, "SensorInterval=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("SensorInterval=") - 1;
	  		while (*temp != ',')
	  		{
	  			receivedData[i] = *temp;
	  			temp++;
	  			i++;
	  		}
	  		uart_sensorInterval = atoi(&receivedData);
	  		
	  		// copy Light
	  		i = 0;
	  		temp = strstr(buf, "Light=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("Light=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_light = atoi(&receivedData);
			
			// copy Humid
	  		i = 0;
	  		temp = strstr(buf, "Humidity=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("Humidity=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_humidity = atoi(&receivedData);
			
			// copy Temp
	  		i = 0;
	  		temp = strstr(buf, "Temperature=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("Temperature=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_temperature = atoi(&receivedData);	
			
			// send the configuration packet over radio
			if(!busyRadio) {
				ConfigurationPacket* sendPkt = (ConfigurationPacket*)(call RadioPacket.getPayload(&pkt, sizeof (ConfigurationPacket)));
					sendPkt->pkt_type = CONFIGURATION_PACKET;
					sendPkt->nodeID = uart_nodeID;
					sendPkt->sensorInterval = uart_sensorInterval;
					sendPkt->light = uart_light;
					sendPkt->humidity = uart_humidity;
					sendPkt->temperature = uart_temperature;
					if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ConfigurationPacket)) == SUCCESS){
						busyRadio = TRUE;
						counter++;
					}
			}
			call Leds.led2Toggle();
			call UartStream.receive(uartString, 92);	  			 		
  		} else if (strstr(buf, "GioConfig") != NULL){
			// local variables
			char receivedData[10];
	  		uint16_t uart_nodeID;
	  		uint8_t uart_gio0, uart_gio1, uart_gio2, uart_gio3;	  		
	  		uint8_t i = 0;  
	  		//copy NodeID		
	  		char* temp = strstr(buf, "NodeID=");
	  		temp = temp + sizeof("NodeID=") - 1;
	  		while (*temp != ',')
	  		{
	  			receivedData[i] = *temp;
	  			temp++;
	  			i++;
	  		}
	  		uart_nodeID = atoi(&receivedData);
			// copy gio0 value		
	  		i = 0;
	  		temp = strstr(buf, "GIO0=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("GIO0=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_gio0 = atoi(&receivedData);
			// copy gio1 value		
	  		i = 0;
	  		temp = strstr(buf, "GIO1=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("GIO1=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_gio1 = atoi(&receivedData);
			// copy gio2 value		
	  		i = 0;
	  		temp = strstr(buf, "GIO2=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("GIO2=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_gio2 = atoi(&receivedData);
			// copy gio3 value		
	  		i = 0;
	  		temp = strstr(buf, "GIO3=");
	  		memset(receivedData, 0, 10); // clear the char array
			temp = temp + sizeof("GIO3=") - 1;
			receivedData[0] = *temp;
	  		receivedData[1] = "\0";
			uart_gio3 = atoi(&receivedData);
			// send the configuration packet over radio
			if(!busyRadio) {
				GioConfigurationPacket* sendPkt = (GioConfigurationPacket*)(call RadioPacket.getPayload(&pkt, sizeof (GioConfigurationPacket)));
					sendPkt->pkt_type = GIO_CONFIGURATION_PACKET;
					sendPkt->nodeID = uart_nodeID;
					sendPkt->gio0_value = uart_gio0;
					sendPkt->gio1_value = uart_gio1;
					sendPkt->gio2_value = uart_gio2;
					sendPkt->gio3_value = uart_gio3;
					if(call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(GioConfigurationPacket)) == SUCCESS){
						busyRadio = TRUE;
						counter++;
					}
			}
			call Leds.led2Toggle();
			call UartStream.receive(uartString, 92);
  		} else
  			call UartStream.receive(uartString, 92);
	}

	async event void UartStream.receivedByte(uint8_t byte){
		// TODO Auto-generated method stub
		busySerial = TRUE;
		//call UartStream.receive(uartString, 92);		
	}

	async event void UartStream.sendDone(uint8_t *buf, uint16_t len, error_t error){
		// TODO Auto-generated method stub
		busySerial = FALSE;
	}
}


