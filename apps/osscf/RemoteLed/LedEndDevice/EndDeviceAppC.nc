#include <Timer.h>
#include "massages.h"

configuration EndDeviceAppC {
}
implementation {
 components MainC;
 components LedsC;
 components EndDeviceC;
 components new TimerMilliC() as TimerSensor;
 components new TimerMilliC() as TimerNetwork;
 components ActiveMessageC;
 components new AMSenderC(SENSORMSG);
 components new AMReceiverC(SENSORMSG);
 components new SensorHumidityC() as SensorHumidity;
 components new SensorLightC() as SensorLight;
 components new SensorTemperatureC() as SensorTemperature;
 components HplMsp430GeneralIOC as PinMap;		//GPIO
 EndDeviceC.GIO0 -> PinMap.Port63;  //For GIO0 or ADC3
 EndDeviceC.GIO1 -> PinMap.Port62;  //For GIO1 or ADC2
 EndDeviceC.GIO2 -> PinMap.Port60;  //For ADC0
 EndDeviceC.GIO3 -> PinMap.Port61;  //For ADC1
  
 EndDeviceC.Boot -> MainC;
 EndDeviceC.Leds -> LedsC;
 EndDeviceC.TimerSensor -> TimerSensor;
 EndDeviceC.TimerNetwork -> TimerNetwork;
 EndDeviceC.Packet -> AMSenderC;
 EndDeviceC.AMPacket -> AMSenderC;
 EndDeviceC.AMSend -> AMSenderC;
 EndDeviceC.AMControl -> ActiveMessageC;
 EndDeviceC.Receive -> AMReceiverC;
 EndDeviceC.Temperature -> SensorTemperature;
 EndDeviceC.Light -> SensorLight;
 EndDeviceC.Humidity -> SensorHumidity;

}
