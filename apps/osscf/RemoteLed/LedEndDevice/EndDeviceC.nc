#include <Timer.h>
#include "massages.h"

#define FLOOR_ID 14
#define ROOM_ID 15
#define NETWORK_INTERVAL 1000
	
module EndDeviceC {
	uses interface Boot;
	uses interface Leds;
	uses interface Timer<TMilli> as TimerSensor;
	uses interface Timer<TMilli> as TimerNetwork;
	uses interface Packet;
	uses interface AMPacket;
	uses interface AMSend;
	uses interface SplitControl as AMControl;
	uses interface Receive;
	uses interface Read<uint16_t> as Light;
	uses interface Read<uint16_t> as Humidity;
	uses interface Read<uint16_t> as Temperature;	
	uses interface HplMsp430GeneralIO as GIO0;
	uses interface HplMsp430GeneralIO as GIO1;
	uses interface HplMsp430GeneralIO as GIO2;
	uses interface HplMsp430GeneralIO as GIO3;
}

implementation {
	bool networkJoined = FALSE;
	bool busy = FALSE;
	bool light,temperature, humidity, volstat = TRUE;
	bool lightReadDone, temperatureReadDone, humidityReadDone;
	message_t pkt;
	uint16_t sensorValue[4];
	uint8_t counter = 0;
	uint16_t sensorInterval = 0;
	uint16_t msgType;
	uint8_t numSensors = 0;
	uint8_t maxSensors = 0;
  	
	void creatEDSensorReport() {
		 call Leds.led2Toggle();		 
		 msgType = ED_SENSOR_REPORT;
		 if(!busy) {
				 EDSensorReport* sendPkt = (EDSensorReport*)(call Packet.getPayload(&pkt, sizeof (EDSensorReport)));
				 sendPkt->pkt_type = ED_SENSOR_REPORT;
				 sendPkt->ED_nodeID = TOS_NODE_ID;
				 sendPkt->roomID = ROOM_ID;
				 sendPkt->sensorValue[0] = sensorValue[0];
				 sendPkt->sensorValue[1] = sensorValue[1];
				 sendPkt->sensorValue[2] = sensorValue[2];				 
				 if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDSensorReport)) == SUCCESS){
					 busy = TRUE;
	 				 numSensors = 0;
				 }
		}
	}
	
	event void Boot.booted() {
		uint8_t state = 0xFF;
		call AMControl.start();

		/*set up GIO0*/
		/* Disable resistor */
		call GIO0.setResistor(MSP430_PORT_RESISTOR_OFF);
		state = call GIO0.getResistor();
		/* Enable pullup resistor */
		call GIO0.setResistor(MSP430_PORT_RESISTOR_PULLUP);
		state = call GIO0.getResistor();
		/* slow PWM */
		call GIO0.makeOutput();
		
		/*set up GIO1*/
		/* Disable resistor */
		call GIO1.setResistor(MSP430_PORT_RESISTOR_OFF);
		state = call GIO1.getResistor();
		/* Enable pullup resistor */
		call GIO1.setResistor(MSP430_PORT_RESISTOR_PULLUP);
		state = call GIO1.getResistor();
		/* slow PWM */
		call GIO1.makeOutput();
		
		/*set up GIO2*/
		/* Disable resistor */
		call GIO2.setResistor(MSP430_PORT_RESISTOR_OFF);
		state = call GIO2.getResistor();
		/* Enable pullup resistor */
		call GIO2.setResistor(MSP430_PORT_RESISTOR_PULLUP);
		state = call GIO2.getResistor();
		/* slow PWM */
		call GIO2.makeOutput();
		
		/*set up GIO3*/
		/* Disable resistor */
		call GIO3.setResistor(MSP430_PORT_RESISTOR_OFF);
		state = call GIO3.getResistor();
		/* Enable pullup resistor */
		call GIO3.setResistor(MSP430_PORT_RESISTOR_PULLUP);
		state = call GIO3.getResistor();
		/* slow PWM */
		call GIO3.makeOutput();
	}
	
	event void TimerNetwork.fired() {
		msgType = ED_JOIN_REQUEST_RESPONSE;
		if(!busy && !networkJoined) {
			EDJoinRequestResponse* sendPkt = (EDJoinRequestResponse*)(call Packet.getPayload(&pkt, sizeof (EDJoinRequestResponse)));
			sendPkt->pkt_type = REQUEST_VAL;
			sendPkt->f_n_ID = FLOOR_ID;
			sendPkt->ED_nodeID = TOS_NODE_ID;
			if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
	 			busy = TRUE;
	 		}
	 	}
	}
	
	event void TimerSensor.fired() {
			call Leds.led1Toggle();
			call Light.read() ;			 
	}
	
	event void Light.readDone(error_t result, uint16_t data) {
			sensorValue[0] = data;
			call Temperature.read() ;
  	}
  	
  	  event void Temperature.readDone(error_t result, uint16_t data) {
			 sensorValue[1] = data;
			 call Humidity.read();
  	}
  	
  	event void Humidity.readDone(error_t result, uint16_t data) {
			sensorValue[2] = data;
			creatEDSensorReport();
  	}
  	  	
	event void AMControl.startDone(error_t error){
		if (error == SUCCESS){
			call TimerNetwork.startPeriodic(1024);			
		}
		else {
			call AMControl.start();	
		}
	}
	
	event void AMControl.stopDone(error_t error){
		// TODO Auto-generated method stub
	}
	

	event void AMSend.sendDone(message_t *msg, error_t error){
		if (&pkt == msg) {
			busy = FALSE;
			if (counter < PACKAGE_REPEATING) {
				switch (msgType) {
					case ED_JOIN_REQUEST_RESPONSE : {
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
		 					busy = TRUE;
		 					counter++;
		 				}
						break;
					}
					case ED_SENSOR_REPORT : {
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDSensorReport)) == SUCCESS){
		 					busy = TRUE;
		 					counter++;
		 				}
						break;
					}
				}
			} else {
				counter = 0;
			}
		}
	}

	void processGio0(bool state){
		if (state)
			call GIO0.set();
		else
			call GIO0.clr();
	}
	
	void processGio1(bool state){
		if (state)
			call GIO1.set();
		else
			call GIO1.clr();
	}
	
	void processGio2(bool state){
		if (state)
			call GIO2.set();
		else
			call GIO2.clr();
	}
	
	void processGio3(bool state){
		if (state)
			call GIO3.set();
		else
			call GIO3.clr();
	}
	
	event message_t * Receive.receive(message_t* msg, void* payload, uint8_t len){
		
		if (len == sizeof(EDJoinRequestResponse)) { 
			EDJoinRequestResponse* receivedPkt = (EDJoinRequestResponse*)payload;
			if ((receivedPkt->pkt_type == RESPONS_VAL) && (receivedPkt->ED_nodeID == TOS_NODE_ID)) {
				call TimerNetwork.stop();
				networkJoined = TRUE;
				msgType = ED_JOIN_REQUEST_RESPONSE;
				if(!busy) {
					EDJoinRequestResponse* sendPkt = (EDJoinRequestResponse*)(call Packet.getPayload(&pkt, sizeof (EDJoinRequestResponse)));
					sendPkt->pkt_type = CONFIRM_VAL;
					sendPkt->f_n_ID = receivedPkt->f_n_ID;
					sendPkt->ED_nodeID = TOS_NODE_ID;
					if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
						busy = TRUE;
						counter++;
					}
					call TimerSensor.startPeriodic(2000);
				}
			}			
		} else if (len == sizeof(GioConfigurationPacket)) {
			GioConfigurationPacket* receivedPkt = (GioConfigurationPacket*)payload;
			if (receivedPkt->pkt_type == GIO_CONFIGURATION_PACKET && receivedPkt->nodeID == TOS_NODE_ID) {
				call Leds.led0Toggle();				
				processGio0(receivedPkt->gio0_value);
				processGio1(receivedPkt->gio1_value);
				processGio2(receivedPkt->gio2_value);
				processGio3(receivedPkt->gio3_value);				
			}
		}
		return msg;
	}
	
	
	
}


