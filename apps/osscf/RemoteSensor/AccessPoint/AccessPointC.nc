#include <Timer.h>
#include "massages.h"

#define FLOOR_ID 14
#define NETWORK_INTERVAL 2000
#define PARENT_QUERY_INTERVAL 20000
	
module AccessPointC {
	uses interface Boot;
	uses interface Leds;
	uses interface Timer<TMilli> as TimerParentQuery;
	uses interface Timer<TMilli> as TimerNetwork;
	uses interface Timer<TMilli> as TimerCollectSensorData;
	uses interface Packet;
	uses interface AMPacket;
	uses interface AMSend;
	uses interface SplitControl as AMControl;
	uses interface Receive;
}

implementation {
	bool busy = FALSE; 
	bool networkJoind = FALSE;
	bool parentIsAlive = FALSE;
	message_t pkt;
	uint16_t parent, gradient;
	uint16_t sensorInterval;
	uint16_t childrenList[50][5];
	uint16_t childrenCounter;
	uint8_t counter = 0;
	uint8_t parentQuery = 0;
	uint16_t msgType;
	uint16_t downstreamRoutingTable[50];
	uint8_t downstreamRoutingTableCounter = 0;

  	bool isInDownstreamRoutingTable(uint8_t nodeID){
  		int x;
  		for (x = 0; x < downstreamRoutingTableCounter; x++){
  			if (downstreamRoutingTable[x] == nodeID) {
  				return TRUE;
  			}
  		}
  		return FALSE;
  	}
  	
  	bool isChild(uint8_t childID) {
  		int x;
  		for (x = 0; x < childrenCounter; x++) {
  			if( childrenList[x][0] == childID){
  				return TRUE;
  			}
  		}
  		return FALSE;
  	}
  	
  	bool isSensorReportReady() {
  		int x;
  		for(x = 0; x < childrenCounter; x++) {
  			if( childrenList[x][1] == FALSE){
  				return FALSE;
  			}
  		}
  		return TRUE;
  	}
  	
  	uint16_t getChild(uint16_t childID) {
  		int x;
  		for(x = 0; x < childrenCounter; x++) {
  			if( childrenList[x][0] == childID){
  				childrenList[x][1] = TRUE;
  				return x;
  			}
  		}
  		return 0;
  	}
  	
  	void creatAccessPointSensorReport (EDSensorReport* receivedPkt) {
  			msgType = ACCESS_POINT_SENSOR_REPORT;
			if(!busy) {
		    	AccessPointSensorReport* sendPkt = (AccessPointSensorReport*)(call Packet.getPayload(&pkt, sizeof (AccessPointSensorReport)));
		 		call Leds.led2Toggle();
		 		sendPkt->pkt_type = ACCESS_POINT_SENSOR_REPORT;
				sendPkt->nextForwardingNode = parent;
				sendPkt->currForwardingNode = TOS_NODE_ID;
				sendPkt->senderID = TOS_NODE_ID;
				sendPkt->floorID = FLOOR_ID;
				sendPkt->roomID = receivedPkt->roomID;
				sendPkt->ED_nodeID = receivedPkt->ED_nodeID;
				sendPkt->sensorValue[0] = receivedPkt->sensorValue[0];
  				sendPkt->sensorValue[1] = receivedPkt->sensorValue[1];
  				sendPkt->sensorValue[2] = receivedPkt->sensorValue[2];
				if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(AccessPointSensorReport)) == SUCCESS){
		 			busy = TRUE;
		 			counter++;
		 		}
		 	}
  	}
	event void Boot.booted() {
 		int x;
 		for (x = 0; x < 50; x++) {
 			childrenList[x][0] = NIL;
 			childrenList[x][1] = NIL;
 			childrenList[x][2] = NIL;
 			childrenList[x][3] = NIL;
 			childrenList[x][4] = NIL;
 			downstreamRoutingTable[x] = NIL;
 		}
 		parent = NIL;
 		gradient = MAX_INT;
 		childrenCounter = 0;
		call AMControl.start();
	}
	
	event void TimerParentQuery.fired() { 
		if(!busy && parentQuery < 3) {
		 	ParentStatusQuery* sendPkt = (ParentStatusQuery*)(call Packet.getPayload(&pkt, sizeof (ParentStatusQuery)));
		 	sendPkt->pkt_type = PSQ_REQUEST;
		 	sendPkt->parentID = parent;
			msgType = PARENT_STATUS_QUERY;
			if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentStatusQuery)) == SUCCESS){
		 		busy = TRUE;
		 		parentQuery++;
		 		counter++;
		 	}
	 	}else if (!busy && parentQuery >= 3) {
	 		ParentDiscovery* sendPkt = (ParentDiscovery*)(call Packet.getPayload(&pkt, sizeof (ParentDiscovery)));
		 	sendPkt->pkt_type = PARENT_DISCOVERY;
		 	sendPkt->senderID = TOS_NODE_ID;
		 	msgType = PARENT_DISCOVERY;
			if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentDiscovery)) == SUCCESS){
		 		busy = TRUE;
		 		parent = NIL;
		    	gradient = MAX_INT;
		 		call Leds.set(gradient);
		 		counter++;
		 	}
		}
	}
	
	event void TimerNetwork.fired() {
		if (!networkJoind){
			msgType = PARENT_DISCOVERY;
			if (!busy) {
			 	ParentDiscovery* sendPkt = (ParentDiscovery*)(call Packet.getPayload(&pkt, sizeof (ParentDiscovery)));
				sendPkt->pkt_type = PARENT_DISCOVERY;
				sendPkt->senderID = TOS_NODE_ID;
				if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentDiscovery)) == SUCCESS){
				 	busy = TRUE;
					counter++;
				}
			}
		 } else {
		 	call TimerNetwork.stop();
		 }
	}
	
	event void TimerCollectSensorData.fired() {
		
	}

	event void AMControl.startDone(error_t error){
		if (error == SUCCESS){
			call TimerNetwork.startPeriodic(NETWORK_INTERVAL);
		}
		else {
			call AMControl.start();	
		}
	}
	
	event void AMControl.stopDone(error_t error){
		// TODO Auto-generated method stub
	}
	

	event void AMSend.sendDone(message_t *msg, error_t error){
		if (&pkt == msg) {
			busy = FALSE;
			if(counter < PACKAGE_REPEATING) {
				switch (msgType) {
					case BEACON_PACKET : {
									if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BeaconPacket)) == SUCCESS){
										busy = TRUE;
										counter++;
									}
									break;
						}
					case ED_JOIN_REQUEST_RESPONSE : {
							if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
											busy = TRUE;
											counter++;
							}
						break;
						}
					case PARENT_STATUS_QUERY : {
							if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentStatusQuery)) == SUCCESS){
											busy = TRUE;
											counter++;
							}
						break;
						}
					case PARENT_DISCOVERY : {
							if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentDiscovery)) == SUCCESS){
											busy = TRUE;
											counter++;
							}
						break;
						}
					case ACCESS_POINT_SENSOR_REPORT : {
							if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(AccessPointSensorReport)) == SUCCESS){
											busy = TRUE;
											counter++;
							}
						break;
						}
					case CONFIGURATION_PACKET : {
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ConfigurationPacket)) == SUCCESS){
											busy = TRUE;
											counter++;
							}
						break;
					}
				}
			} else {
				counter = 0;
			}
		}	
	}

	event message_t * Receive.receive(message_t* msg, void* payload, uint8_t len){
		if(len == sizeof(BeaconPacket)) {
			BeaconPacket* receivedPkt = (BeaconPacket*)payload;
			if (receivedPkt->pkt_type == BEACON_PACKET) {
				if ( (parent == NIL && gradient == MAX_INT) || (receivedPkt->gradientVal < gradient) || ((receivedPkt->senderID == parent) && (receivedPkt->gradientVal != (gradient - 1))) ) {
					call TimerParentQuery.startPeriodic(PARENT_QUERY_INTERVAL);
					gradient = receivedPkt->gradientVal;
					parent = receivedPkt->senderID;
					networkJoind = TRUE;
					msgType = BEACON_PACKET;
					if(!busy) {
					 	BeaconPacket* sendPkt = (BeaconPacket*)(call Packet.getPayload(&pkt, sizeof (BeaconPacket)));
						sendPkt->pkt_type = BEACON_PACKET;
						sendPkt->gradientVal = gradient + 1;
						sendPkt->senderID = TOS_NODE_ID;
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BeaconPacket)) == SUCCESS){
					 		busy = TRUE;
					 		counter++;
					 	}
		 			}
				}
			}
		}
		
		if (len == sizeof(EDJoinRequestResponse)) {
			EDJoinRequestResponse* receivedPkt = (EDJoinRequestResponse*)payload;
			if ((receivedPkt->pkt_type == REQUEST_VAL) && (receivedPkt->f_n_ID == FLOOR_ID)) {
			msgType = ED_JOIN_REQUEST_RESPONSE;
				if(!busy) {
					EDJoinRequestResponse* sendPkt = (EDJoinRequestResponse*)(call Packet.getPayload(&pkt, sizeof (EDJoinRequestResponse)));
					sendPkt->pkt_type = RESPONS_VAL;
		 			sendPkt->f_n_ID = TOS_NODE_ID;
		 			sendPkt->ED_nodeID = receivedPkt->ED_nodeID;
		 			call Leds.set(sendPkt->f_n_ID);
					if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
		 				busy = TRUE;
		 				counter++;
		 			}
		 		}
			}
			if ((receivedPkt->pkt_type == CONFIRM_VAL) &&  (receivedPkt->f_n_ID == TOS_NODE_ID)) {
				childrenList[childrenCounter][0] = receivedPkt->ED_nodeID;
				childrenCounter++;
				msgType = ACCESS_POINT_SENSOR_REPORT;
				if(!busy) {
					AccessPointSensorReport* sendPkt = (AccessPointSensorReport*)(call Packet.getPayload(&pkt, sizeof (AccessPointSensorReport)));
					sendPkt->pkt_type = ACCESS_POINT_SENSOR_REPORT;
					sendPkt->senderID = TOS_NODE_ID;
					sendPkt->floorID = FLOOR_ID;
					sendPkt->ED_nodeID = receivedPkt->ED_nodeID;
					sendPkt->sensorValue[0] = 0;
					sendPkt->sensorValue[1] = 0;
					sendPkt->sensorValue[2] = 0;
					sendPkt->nextForwardingNode = parent;
					sendPkt->currForwardingNode = TOS_NODE_ID;
					if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(AccessPointSensorReport)) == SUCCESS){
					 	busy = TRUE;
					 	counter++;
					}
		 		}
				
			} 	
		}
		
		if (len == sizeof(ParentStatusQuery)) {
			ParentStatusQuery* receivedPkt = (ParentStatusQuery*)payload;
			if ((receivedPkt->pkt_type == PSQ_REQUEST) && (receivedPkt->parentID == TOS_NODE_ID)) {
				msgType = PARENT_STATUS_QUERY;
				if(!busy) {
				   	ParentStatusQuery* sendPkt = (ParentStatusQuery*)(call Packet.getPayload(&pkt, sizeof (ParentStatusQuery)));
					sendPkt->pkt_type = PSQ_RESPONS;
		 			sendPkt->parentID = TOS_NODE_ID;
					if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ParentStatusQuery)) == SUCCESS){
		 				busy = TRUE;
		 				counter++;
		 			}
		 		}
			}	
			if (receivedPkt->pkt_type == PSQ_RESPONS && receivedPkt->parentID == parent) {
				call TimerParentQuery.stop();
				parentQuery = 0;
				call TimerParentQuery.startPeriodic(PARENT_QUERY_INTERVAL);
			} 	
		}
		
		if (len == sizeof(ParentDiscovery)) {
			ParentDiscovery* receivedPkt = (ParentDiscovery*)payload;
			if (receivedPkt->pkt_type == PARENT_DISCOVERY && receivedPkt->senderID == parent) {
			} else if (parent != NIL && gradient != MAX_INT) {
				msgType = BEACON_PACKET;
				if(!busy) {
					BeaconPacket* sendPkt = (BeaconPacket*)(call Packet.getPayload(&pkt, sizeof (BeaconPacket)));
					sendPkt->pkt_type = BEACON_PACKET;
					sendPkt->gradientVal = gradient + 1;
					sendPkt->senderID = TOS_NODE_ID;
					if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BeaconPacket)) == SUCCESS){
					 	busy = TRUE;
					 	counter++;
					 }
		 		}
			}
	 	}
		
		if (len == sizeof(EDSensorReport)) {
			EDSensorReport* receivedPkt = (EDSensorReport*)payload;
			if (receivedPkt->pkt_type == ED_SENSOR_REPORT && isChild(receivedPkt->ED_nodeID)) {
				call Leds.led0Toggle();
				creatAccessPointSensorReport(receivedPkt);
			}
		}
		
		if (len == sizeof(AccessPointSensorReport)) {
			AccessPointSensorReport* receivedPkt = (AccessPointSensorReport*)payload;
			if (receivedPkt->pkt_type == ACCESS_POINT_SENSOR_REPORT && receivedPkt->nextForwardingNode == TOS_NODE_ID) {
				if (!isInDownstreamRoutingTable(receivedPkt->senderID)) {
					downstreamRoutingTable[downstreamRoutingTableCounter] = receivedPkt->senderID;
					downstreamRoutingTableCounter++;
				}
				if (!isInDownstreamRoutingTable(receivedPkt->currForwardingNode)) {
					downstreamRoutingTable[downstreamRoutingTableCounter] = receivedPkt->currForwardingNode;
					downstreamRoutingTableCounter++;
				}
				msgType = ACCESS_POINT_SENSOR_REPORT;
				if(!busy) {
					AccessPointSensorReport* sendPkt = (AccessPointSensorReport*)(call Packet.getPayload(&pkt, sizeof (AccessPointSensorReport)));
					sendPkt->pkt_type = receivedPkt->pkt_type;
					sendPkt->senderID = receivedPkt->senderID;
					sendPkt->floorID = receivedPkt->floorID;
					sendPkt->roomID = receivedPkt->roomID;
					sendPkt->ED_nodeID = receivedPkt->ED_nodeID;
					sendPkt->sensorValue[0] = receivedPkt->sensorValue[0];
					sendPkt->sensorValue[1] = receivedPkt->sensorValue[1];
					sendPkt->sensorValue[2] = receivedPkt->sensorValue[2];
					sendPkt->nextForwardingNode = parent;
					sendPkt->currForwardingNode = TOS_NODE_ID;
					if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(AccessPointSensorReport)) == SUCCESS){
					 	busy = TRUE;
					 	counter++;
					}
		 		}			
			}
		}
		
		if (len == sizeof(ConfigurationPacket)) {
			ConfigurationPacket* receivedPkt = (ConfigurationPacket*)payload;
			if (receivedPkt->pkt_type == CONFIGURATION_PACKET) {
				call Leds.set(receivedPkt->nodeID);
				if (isInDownstreamRoutingTable(receivedPkt->nodeID) || isChild(receivedPkt->nodeID)) {
					msgType = CONFIGURATION_PACKET;
					if(!busy) {
						ConfigurationPacket* sendPkt = (ConfigurationPacket*)(call Packet.getPayload(&pkt, sizeof (ConfigurationPacket)));
						sendPkt->pkt_type = receivedPkt->pkt_type;
						sendPkt->nodeID = receivedPkt->nodeID;
						sendPkt->sensorInterval = receivedPkt->sensorInterval;
						sendPkt->light = receivedPkt->light;
						sendPkt->humidity = receivedPkt->humidity;
						sendPkt->temperature = receivedPkt->temperature;
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ConfigurationPacket)) == SUCCESS){
						 	busy = TRUE;
						 	counter++;
						}
					}	
				}
			}
		}
		return msg;
	}
}


