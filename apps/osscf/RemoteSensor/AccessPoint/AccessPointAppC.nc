#include <Timer.h>
#include "massages.h"

configuration AccessPointAppC
{
}
implementation
{
  components MainC; 
  components LedsC;
  components AccessPointC;
  components new TimerMilliC() as TimerParentQuery;
  components new TimerMilliC() as TimerNetwork;
  components new TimerMilliC() as TimerCollectSensorData;
  components ActiveMessageC;
  components new AMSenderC(SENSORMSG);
  components new AMReceiverC(SENSORMSG);

  AccessPointC -> MainC.Boot;
  AccessPointC.Boot -> MainC;
  AccessPointC.Leds -> LedsC;
  AccessPointC.TimerParentQuery -> TimerParentQuery;
  AccessPointC.TimerNetwork -> TimerNetwork;
  AccessPointC.TimerCollectSensorData -> TimerCollectSensorData;
  AccessPointC.Packet -> AMSenderC;
  AccessPointC.AMPacket -> AMSenderC;
  AccessPointC.AMSend -> AMSenderC;
  AccessPointC.AMControl -> ActiveMessageC;
  AccessPointC.Receive -> AMReceiverC;
}

