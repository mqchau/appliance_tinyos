// $Id: TestSerialAppC.nc,v 1.6 2010/06/29 22:07:25 scipio Exp $

/**
 * Application to test that the TinyOS java toolchain can communicate
 * with motes over the serial port. The application sends packets to
 * the serial port at 1Hz: the packet contains an incrementing
 * counter. When the application receives a counter packet, it
 * displays the bottom three bits on its LEDs. This application is
 * very similar to RadioCountToLeds, except that it operates over the
 * serial port. There is Java application for testing the mote
 * application: run TestSerial to print out the received packets and
 * send packets to the mote.
 *
 *  @author Nhat-Quang Ton
 *  @author Ivo
 *  
 *  @date   April 20, 2013
 *
 **/

#include "massages.h"

configuration TestSerialAppC {}
implementation {
  components TestSerialC as App, LedsC, MainC;
//  components SerialActiveMessageC as SerialAM;
  components ActiveMessageC as RadioAM;
  components new AMSenderC(SENSORMSG) as RadioSender;
  components new AMReceiverC(SENSORMSG) as RadioReceiver;
  components new TimerMilliC();
  components new Msp430Uart1C() as Uart;

  App.Boot -> MainC.Boot;
//  App.SerialControl -> SerialAM;
//  App.SerialReceive -> SerialAM.Receive[AM_CONFIGURATIONPACKET];
//  App.SerialSend -> SerialAM.AMSend[AM_CONFIGURATIONPACKET];
  App.Leds -> LedsC;
  App.MilliTimer -> TimerMilliC;
//  App.SerialPacket -> SerialAM;
  
  App.RadioPacket -> RadioSender;
  App.RadioAMPacket -> RadioReceiver;
  App.RadioSend -> RadioSender;
  App.RadioAMControl -> RadioAM;
  App.RadioReceive -> RadioReceiver;
  
  App.Resource -> Uart.Resource;
//  App.ResourceRequested	-> Uart;
//  App.UartByte -> Uart;
  App.UartStream -> Uart.UartStream;
  App.Msp430UartConfigure <- Uart.Msp430UartConfigure;  
}


