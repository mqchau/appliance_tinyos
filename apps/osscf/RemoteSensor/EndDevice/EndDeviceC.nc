#include <Timer.h>
#include "massages.h"

#define FLOOR_ID 14
#define ROOM_ID 15
#define NETWORK_INTERVAL 2000
	
module EndDeviceC {
	uses interface Boot;
	uses interface Leds;
	uses interface Timer<TMilli> as TimerSensor;
	uses interface Timer<TMilli> as TimerNetwork;
	uses interface Packet;
	uses interface AMPacket;
	uses interface AMSend;
	uses interface SplitControl as AMControl;
	uses interface Receive;
	uses interface Read<uint16_t> as Light;
	uses interface Read<uint16_t> as Humidity;
	uses interface Read<uint16_t> as Temperature;
	uses interface Read<uint16_t> as Vref;
}

implementation {
	bool networkJoined = FALSE;
	bool busy = FALSE;
	bool light,temperature, humidity, volstat = TRUE;
	bool lightReadDone, temperatureReadDone, humidityReadDone;
	message_t pkt;
	uint16_t sensorValue[4];
	uint8_t counter = 0;
	uint16_t sensorInterval = 0;
	uint16_t msgType;
	uint8_t numSensors = 0;
	uint8_t maxSensors = 0;
  	
	void creatEDSensorReport() {
		 call Leds.led2Toggle();
		 if (!light) {sensorValue[0] = 0;}
		 if (!humidity) {sensorValue[2] = 0;}
		 if (!temperature) {sensorValue[1] = 0;}
		 msgType = ED_SENSOR_REPORT;
		 if(!busy) {
				 EDSensorReport* sendPkt = (EDSensorReport*)(call Packet.getPayload(&pkt, sizeof (EDSensorReport)));
				 sendPkt->pkt_type = ED_SENSOR_REPORT;
				 sendPkt->ED_nodeID = TOS_NODE_ID;
				 sendPkt->roomID = ROOM_ID;
				 sendPkt->sensorValue[0] = sensorValue[0];
				 sendPkt->sensorValue[1] = sensorValue[1];
				 sendPkt->sensorValue[2] = sensorValue[2];
				 //sendPkt->sensorValue[3] = sensorValue[3];
				 if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDSensorReport)) == SUCCESS){
					 busy = TRUE;
	 				 numSensors = 0;
				 }
		}
	}
	
	event void Boot.booted() {
		call AMControl.start();
	}
	
	event void TimerNetwork.fired() {
		msgType = ED_JOIN_REQUEST_RESPONSE;
		if(!busy && !networkJoined) {
			EDJoinRequestResponse* sendPkt = (EDJoinRequestResponse*)(call Packet.getPayload(&pkt, sizeof (EDJoinRequestResponse)));
			sendPkt->pkt_type = REQUEST_VAL;
			sendPkt->f_n_ID = FLOOR_ID;
			sendPkt->ED_nodeID = TOS_NODE_ID;
			if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
	 			busy = TRUE;
	 		}
	 	}
	}
	
	event void TimerSensor.fired() {
			call Leds.led1Toggle();
			 if (volstat)
			 	//call Vref.read();
			 if (light) {
			   	call Light.read() ;
			 }
			 if (temperature) {
			   	call Temperature.read() ;
			 }
			 if (humidity) {
			   	call Humidity.read();
			 }
			 
	}
	
	event void Light.readDone(error_t result, uint16_t data) {
    	if (result != SUCCESS) {
			//report_problem();
		}else {
			sensorValue[0] = data;
			lightReadDone = TRUE;
			if(++numSensors == maxSensors) {
				creatEDSensorReport();
			}
		}
  	}
  	
  	  event void Temperature.readDone(error_t result, uint16_t data) {
    	if (result != SUCCESS) {
			//report_problem();
		}else {
			 sensorValue[1] = data;
			 temperatureReadDone = TRUE;
			 if(++numSensors == maxSensors) {
			 	creatEDSensorReport();
			 }
		}
  	}
  	
  	event void Humidity.readDone(error_t result, uint16_t data) {
    	if (result != SUCCESS) {
			//report_problem();
		}else {
			sensorValue[2] = data;
			humidityReadDone = TRUE;
			if(++numSensors == maxSensors) {
			 	creatEDSensorReport();
			 }
		}
  	}
  	
  	event void Vref.readDone(error_t result, uint16_t val){
		if (result != SUCCESS) {
			//report_problem();
		}else {
			sensorValue[3] = val;
			if(++numSensors == maxSensors) {
				creatEDSensorReport();
			}
		}
	}
  	
	event void AMControl.startDone(error_t error){
		if (error == SUCCESS){
			call TimerNetwork.startPeriodic(NETWORK_INTERVAL);			
		}
		else {
			call AMControl.start();	
		}
	}
	
	event void AMControl.stopDone(error_t error){
		// TODO Auto-generated method stub
	}
	

	event void AMSend.sendDone(message_t *msg, error_t error){
		if (&pkt == msg) {
			busy = FALSE;
			if (counter < PACKAGE_REPEATING) {
				switch (msgType) {
					case ED_JOIN_REQUEST_RESPONSE : {
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
		 					busy = TRUE;
		 					counter++;
		 				}
						break;
					}
					case ED_SENSOR_REPORT : {
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDSensorReport)) == SUCCESS){
		 					busy = TRUE;
		 					counter++;
		 				}
						break;
					}
				}
			} else {
				counter = 0;
			}
		}
	}

	event message_t * Receive.receive(message_t* msg, void* payload, uint8_t len){
		
		if (len == sizeof(EDJoinRequestResponse)) { 
			EDJoinRequestResponse* receivedPkt = (EDJoinRequestResponse*)payload;
			if ((receivedPkt->pkt_type == RESPONS_VAL) && (receivedPkt->ED_nodeID == TOS_NODE_ID)) {
				call TimerNetwork.stop();
				networkJoined = TRUE;
				msgType = ED_JOIN_REQUEST_RESPONSE;
				if(!busy) {
			 			EDJoinRequestResponse* sendPkt = (EDJoinRequestResponse*)(call Packet.getPayload(&pkt, sizeof (EDJoinRequestResponse)));
			 			sendPkt->pkt_type = CONFIRM_VAL;
			 			sendPkt->f_n_ID = receivedPkt->f_n_ID;
			 			sendPkt->ED_nodeID = TOS_NODE_ID;
						if(call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(EDJoinRequestResponse)) == SUCCESS){
	 						busy = TRUE;
	 						counter++;
	 					}
	 				}
				}
		 	}
		
		
		if (len == sizeof(ConfigurationPacket)) {
			ConfigurationPacket* receivedPkt = (ConfigurationPacket*)payload;
			if (receivedPkt->pkt_type == CONFIGURATION_PACKET && receivedPkt->nodeID == TOS_NODE_ID) {
				call Leds.led0Toggle();
				sensorInterval = receivedPkt->sensorInterval;
				light = receivedPkt->light;
				temperature = receivedPkt->temperature;
				humidity = receivedPkt->humidity;
				maxSensors = light + temperature + humidity ;
				call TimerSensor.startPeriodic(sensorInterval*1024);
			}
		}
		return msg;
	}

	
}


