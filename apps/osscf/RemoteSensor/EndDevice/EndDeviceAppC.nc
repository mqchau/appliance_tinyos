#include <Timer.h>
#include "massages.h"

configuration EndDeviceAppC {
}
implementation {
 components MainC;
 components LedsC;
 components EndDeviceC;
 components new TimerMilliC() as TimerSensor;
 components new TimerMilliC() as TimerNetwork;
 components ActiveMessageC;
 components new AMSenderC(SENSORMSG);
 components new AMReceiverC(SENSORMSG);
 components new SensorHumidityC() as SensorHumidity;
 components new SensorLightC() as SensorLight;
 components new SensorTemperatureC() as SensorTemperature;
 components new Msp430InternalVoltageC() as SensorVref;  // Voltage    
  
 EndDeviceC.Vref -> SensorVref;	
 EndDeviceC.Boot -> MainC;
 EndDeviceC.Leds -> LedsC;
 EndDeviceC.TimerSensor -> TimerSensor;
 EndDeviceC.TimerNetwork -> TimerNetwork;
 EndDeviceC.Packet -> AMSenderC;
 EndDeviceC.AMPacket -> AMSenderC;
 EndDeviceC.AMSend -> AMSenderC;
 EndDeviceC.AMControl -> ActiveMessageC;
 EndDeviceC.Receive -> AMReceiverC;
 EndDeviceC.Temperature -> SensorTemperature;
 EndDeviceC.Light -> SensorLight;
 EndDeviceC.Humidity -> SensorHumidity;
}
